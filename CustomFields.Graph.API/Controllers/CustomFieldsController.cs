﻿using CustomFields.Graph.API.Services;
using CustomFields.Graph.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CustomFields.Graph.API.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class CustomFieldsController : ControllerBase
	{
		private readonly ISearchGraph _graph;
		public CustomFieldsController(ISearchGraph graph)
		{
			_graph = graph;
		}

		[HttpGet]
		public IEnumerable<CustomFieldView> Get([FromQuery]string name)
		{
			return _graph.RetrieveCustomFields(name);
		}
	}
}
