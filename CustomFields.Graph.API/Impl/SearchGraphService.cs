﻿using CustomFields.Graph.API.Services;
using CustomFields.Graph.ViewModels;
using System.Collections.Generic;

namespace CustomFields.Graph.API.Impl
{
	/// <summary>
	/// Search Graph service that will run as singleton
	/// </summary>
	public class SearchGraphService : ISearchGraph
	{
		public SearchGraphService()
		{
			// initialize Graph Library
			Library.InitializeGraph();
		}
		public IEnumerable<CustomFieldView> RetrieveCustomFields(string name)
		{
			// the first time it gets called, this method will index all fields with [Index]-attribute in the GraphModels/PatentSight.tsl file
			return Library.RetrieveCustomFields(name);
		}
	}
}
