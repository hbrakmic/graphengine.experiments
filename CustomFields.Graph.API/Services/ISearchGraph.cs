﻿using CustomFields.Graph.ViewModels;
using System.Collections.Generic;

namespace CustomFields.Graph.API.Services
{
	/// <summary>
	/// Interface that defines the Search Graph service
	/// </summary>
	public interface ISearchGraph
	{
		IEnumerable<CustomFieldView> RetrieveCustomFields(string name);
	}
}
