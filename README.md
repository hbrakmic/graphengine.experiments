#### PatentSight - GraphEngine - Experiment

This project uses the [GraphEngine](https://www.graphengine.io/) developed by [Microsoft Research Project](https://www.microsoft.com/en-us/research/project/trinity/) "Trinity"

Before starting to tinker with the code, it is strongly recommended to read [the initial white paper](https://www.microsoft.com/en-us/research/publication/trinity-a-distributed-graph-engine-on-a-memory-cloud/) that describes the design and base mechanisms of Trinity (*this was the original name of* "GraphEngine"). 

![solution_list](./Images/solution_list.png)

#### Demo

Start the web app from `Presentation/CustomFields.Graph.API` and use the swagger UI interface to call query for indexed Custom Fields.

![swagger_query](./Images/swagger_query.png)

The search term is "name" and it maps to randomly generated "custom field names", which are basically a combination of "Custom Field" and a city name from US.

**Notice**: the very first search will kick off an index-build. This can take a few moments. Be patient. This variant of GraphEngine runs in `embedded mode`. A real solution would run in a MemoryCloud that spans over multiple machines. 

When the app starts, it generates tens of thousands of different Patent Documents, Custom Fields, Linked Values and Patent Families.

![swagger_results](./Images/swagger_results.png)

#### Infrastructure

When the web app starts, it initializes the Search Service. This service offers a data retrieval method called `RetrieveCustomFields` that expects a string parameter.

In the background, however, is the GrapEngine already running and this is where the true search happens. GraphEngine is located in a separate .NET Core Library Project called `CustomFields.Graph` (the name is maybe a bit misleading, because it could search for many other things, but it's acceptable as a showcase.)

Another important thing to mention is the **model definition** in the `GraphModels/PatentSight.tsl` file. This is where the GraphEngine will look into to create proxy classes, protocols, accessors and messages.

![tsl_file](./Images/tsl_file.png)

Any GraphEngine-project must have a proper TSL (Trinity Specification Language) file.

