﻿using System.Collections.Generic;

namespace CustomFields.Graph.ViewModels
{
	public class CustomFieldView
	{
		public long CustomFieldID { get; set; }

		public IList<CustomFieldValueView> LinkedValues { get; set; }

		public IList<PatentFamilyView> PatentFamilies { get; set; }

		public string Description { get; set; }

		public string Name { get; set; }
	}
}
