﻿using System;

namespace CustomFields.Graph.ViewModels
{
	public class PatentFamilyView
	{
        public int PatentFamilyID { get; set; }

        public string DefaultRepresentativeDocumentNumber { get; set; }

        public DateTime? FirstDateFiling { get; set; }

        public DateTime? FirstDatePublication { get; set; }

        public string TitleEN { get; set; }

        public string AbstractEN { get; set; }

        public PatentDocumentView MatchedBy { get; set; }

    }
}
