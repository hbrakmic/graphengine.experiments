﻿using System;

namespace CustomFields.Graph.ViewModels
{
	public class PatentDocumentView
	{
		public string DocumentNumber { get; set; }

		public DateTime? DateFiling { get; set; }

		public DateTime? DatePublication { get; set; }
	}
}
