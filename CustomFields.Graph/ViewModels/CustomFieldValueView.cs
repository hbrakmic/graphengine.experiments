﻿using System;

namespace CustomFields.Graph.ViewModels
{
	public class CustomFieldValueView
	{
        public int ValueID { get; set; }

        public long CustomFieldID { get; set; }

        public string? TextValue { get; set; }
        public double? NumericValue { get; set; }

        public int PatentFamilyID { get; set; }

        public PatentFamilyView PatentFamily { get; set; }

        public DateTime DateUpdated { get; set; }

        public long UpdatedByUserID { get; set; }

    }
}
