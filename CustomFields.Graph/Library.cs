﻿using CustomFields.Graph.ViewModels;
using RandomDataGenerator.FieldOptions;
using RandomDataGenerator.Randomizers;
using System;
using System.Collections.Generic;
using Trinity;

namespace CustomFields.Graph
{
	public class Library
	{
		/// <summary>
		/// Defines nodes and their relations (edges) in a graph
		/// Currently, we simply generate a bunch of patent documents and various custom fields, patent families and linked values.
		/// </summary>
		public unsafe static void InitializeGraph()
		{
			var cfId = RandomizerFactory.GetRandomizer(new FieldOptionsLong());
			var docNo = RandomizerFactory.GetRandomizer(new FieldOptionsLong());
			var patFamId = RandomizerFactory.GetRandomizer(new FieldOptionsInteger());
			var lvId = RandomizerFactory.GetRandomizer(new FieldOptionsInteger());
			var randDbl = RandomizerFactory.GetRandomizer(new FieldOptionsDouble { UseNullValues = false });
			var randCityName = RandomizerFactory.GetRandomizer(new FieldOptionsCity {  UseNullValues = false });
			var randWords = RandomizerFactory.GetRandomizer(new FieldOptionsTextWords { UseNullValues = false });
			var randLorem = RandomizerFactory.GetRandomizer(new FieldOptionsTextLipsum { UseNullValues = false });

			TrinityConfig.CurrentRunningMode = RunningMode.Embedded;
			TrinityConfig.StorageRoot = "C:\\temp\\graphengine";

			var memory = new PatentSightData(new List<long>());

			int max = 10000;

			for (int i = 0; i < max; i++)
			{
				var patDoc = new PatentDocument
				(
					id: docNo.Generate().Value,
					DateFiling: DateTime.UtcNow,
					DatePublication: DateTime.UtcNow.AddYears(-2),
					DocumentNumber: $"DOC_{docNo.Generate().Value}"
				);

				Global.LocalStorage.SavePatentDocument(patDoc);

				for (int j = 0; j < 10; j++)
				{
					var cf = new CustomField
					(
						id: cfId.Generate().Value,
						Name: $"Custom Field: {randCityName.Generate()}",
						Description: randLorem.Generate(),
						LinkedValues: new List<long> { },
						PatentFamilies: new List<long> { }
					);

					var patFam = new PatentFamily
					(
						id: patFamId.Generate().Value,
						TitleEN: $"Title: {randWords.Generate()}",
						AbstractEN: randWords.Generate(),
						DefaultRepresentativeDocumentNumber: randWords.Generate(),
						FirstDateFiling: DateTime.UtcNow.AddYears(-3),
						FirstDatePublication: DateTime.UtcNow.AddYears(-2),
						MatchedByPatentDocument: patDoc.CellId
					);

					cf.PatentFamilies.Add(patFam.CellId);

					Global.LocalStorage.SavePatentFamily(patFam);

					for (int k = 0; k < 3; k++)
					{
						var lnVal = new LinkedValue
						(
							id: lvId.Generate().Value,
							NumericValue: randDbl.Generate().Value,
							TextValue: randWords.Generate(),
							PatentFamily: patFam.CellId
						);

						

						lnVal.CustomField = cf.CellId;
						cf.LinkedValues.Add(lnVal.CellId);

						Global.LocalStorage.SaveLinkedValue(lnVal);
					}

					memory.CustomFields.Add(cf.CellId);

					Global.LocalStorage.SaveCustomField(cf);
				}
				
			}
			Global.LocalStorage.SavePatentSightData(memory);
			Global.LocalStorage.SaveStorage();
		}

		/// <summary>
		/// Dummy retrieval method that expects a "custom field name" as its only parameter.
		/// </summary>
		/// <param name="name">A US-American city name</param>
		/// <returns>A collection of custom fields that has the given city name as their "name"</returns>
		public static IEnumerable<CustomFieldView> RetrieveCustomFields(string name)
		{
			var customFieldViews = new List<CustomFieldView>();

			var cellIds = Index.SubstringQuery(Index.CustomField.Name, name);
			foreach (var id in cellIds)
			{
				using(var cf = Global.LocalStorage.UseCustomField(id)) // iterate over each CustomField
				{
					var cfv = new CustomFieldView
					{
						CustomFieldID = cf.id,
						Name = cf.Name,
						Description = cf.Description,
						LinkedValues = new List<CustomFieldValueView>(),
						PatentFamilies = new List<PatentFamilyView>()
					};

					cf.LinkedValues.ForEach((cellId) =>
					{
						using (var lv = Global.LocalStorage.UseLinkedValue(cellId)) // iterate over each Linked Value that belongs to a CustomField
						{
							cfv.LinkedValues.Add(new CustomFieldValueView
							{
								CustomFieldID = cf.id,
								TextValue = lv.TextValue,
								NumericValue = lv.NumericValue,
								ValueID = lv.id
							});
						}
					});

					cf.PatentFamilies.ForEach((cellId) =>
					{
						long patentDocId = 0;
						using(var pf = Global.LocalStorage.UsePatentFamily(cellId))
						{
							patentDocId = pf.MatchedByPatentDocument;
							
							cfv.PatentFamilies.Add(new PatentFamilyView
							{
								PatentFamilyID = pf.id,
								TitleEN = pf.TitleEN,
								AbstractEN = pf.AbstractEN,
								DefaultRepresentativeDocumentNumber = pf.DefaultRepresentativeDocumentNumber,
								FirstDateFiling = pf.FirstDateFiling,
								FirstDatePublication = pf.FirstDatePublication
							});
						}

						PatentDocumentView patDocView = null;
						using (var patDoc = Global.LocalStorage.UsePatentDocument(patentDocId))
						{
							patDocView = new PatentDocumentView
							{
								DateFiling = patDoc.DateFiling,
								DatePublication = patDoc.DatePublication,
								DocumentNumber = patDoc.DocumentNumber
							};
						}

						cfv.PatentFamilies[cfv.PatentFamilies.Count-1].MatchedBy = patDocView;

					});
					customFieldViews.Add(cfv);
				}
			}
			return customFieldViews;
		}
	}
}
